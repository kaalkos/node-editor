var express = require('express')
var router = require('express').Router();

// import api functions
var policies = require('./API/policies');

var ratePlans = require('./API/ratePlans');

var rates = require('./API/rates');

var availabilities = require('./API/availabilities');

var cmg = require('./API/cmg');

//
//
// FETCHING FROM DB
//
//

router.post('/user/deposit', policies.fetchDeposit);

router.post('/user/cancellation', policies.fetchCancellation);

router.post('/user/children', policies.fetchChildren);


router.post('/user/deposits', ratePlans.deposits);

router.post('/user/cancellations', ratePlans.cancellations);

router.post('/user/ratePlans', ratePlans.fetchRatePlans);


router.post('/user/ratePlans', rates.fetchRatePlans);

router.post('/user/seasons', rates.fetchSeasons);

router.post('/user/deposits', rates.fetchDeposits);

router.post('/user/cancellations', rates.fetchCancellations);

router.post('/user/rates', rates.fetchRates);

// fetching from CMG API 

router.post('/user/property', cmg.fetchProperty);

router.post('/user/rooms', cmg.fetchRooms);


//
//
// SENDING
//
//

router.post('/databaseSend/deposit', policies.sendDeposit);

router.post('/databaseSend/children', policies.sendChildren);

router.post('/databaseSend/cancellation', policies.sendCancellation);


router.post('/databaseSend/ratePlan', ratePlans.sendRatePlan);

router.post('/databaseSend/products', ratePlans.sendProduct);

router.post('/databaseSend/season', rates.sendSeason);

router.post('/databaseSend/rates', rates.sendRates);

router.post('/databaseSend/availabilities', availabilities.createAvailability);


//
//
// DELETING
//
//

router.post('/databaseDelete/deposit',policies.deleteDeposit);

router.post('/databaseDelete/cancellation', policies.deleteCancellation);

router.post('/databaseDelete/children', policies.deleteChildren);

router.post('/databaseDelete/depositFindRelated', policies.findRelatedToDeposit);	

router.post('/databaseDelete/cancellationFindRelated', policies.findRelatedToCancellation);


router.post('/databaseDelete/ratePlan', ratePlans.deleteRatePlan);


router.post('/databaseDelete/season', rates.deleteSeason);



//
//
// UPDATING
//
//

router.post('/databaseUpdate/deposit', policies.updateDeposit);

router.post('/databaseUpdate/cancellation', policies.updateCancellation);

router.post('/databaseUpdate/children', policies.updateChildren);


router.post('/databaseUpdate/ratePlan', ratePlans.updateRatePlan);


router.post('/databaseUpdate/season', rates.updateSeason);

router.post('/databaseUpdate/ratePlan', rates.updateRatePlan);

router.post('/databaseUpdate/rates', rates.updateRates);




// exports all of the routing functions to the main file
module.exports = router



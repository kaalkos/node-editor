var fs = require('fs');
var https = require('https');
var express = require('express');
var bodyParser = require('body-parser');
var config = require('./config.json');

// import all routes
var routes = require('./routes');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('resources'));

app.use(function(req, res, next) {
	var origin = req.headers.origin;
	var allowedOrigins = config.node.allowedDomains;
	
	if(allowedOrigins.indexOf(origin) > -1){
		res.setHeader('Access-Control-Allow-Origin', origin);
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");		
	}

   next();
});

// implement routes
app.use('/', routes);

//providing views
app.get('/',function(req, res){
 res.sendFile(__dirname + '/' + 'index.html');
});


https.createServer({
	key: fs.readFileSync(config.node.certs.key),
	cert: fs.readFileSync(config.node.certs.cert)
	}, app);
	app.listen(config.node.port, config.node.host, function (err) {
		if(err){
			fs.writeFileSync('error.log',new Date() + ' create server error \n',{'flag': 'a'});
			fs.writeFileSync('eroor.log', err + '\n\n',{'flag': 'a'});
			return console.log(err)
		};
		
		console.log('server is running on port: ' + config.node.port);   
});


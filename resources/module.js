var app = angular.module('onboarding',['ngRoute']);

app.config(['$routeProvider',function($routeProvider){

	$routeProvider
	.when('/policies',
	{
		templateUrl: '/policies.html',
		controller: 'policiesController'
	})
	.when('/rateplans',
	{
		templateUrl: '/ratePlan.html',
		controller: 'ratePlanController'
	})
	.when('/rates',
	{
		templateUrl: '/rates.html',
		controller: 'ratesController'
	});
	
}]);


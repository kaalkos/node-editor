app.controller('ratePlanController',['$scope','$rootScope','$http','$location','$window',
	function($scope,$rootScope,$http,$location,$window){
	
	var hotelId = 1111;
    var userId = 1234;

	$scope.ratePlan = [];


	$scope.ratePlanVisible = false;
	$scope.isEdited = 'false';
	$scope.ratePlanForm = {};

    $scope.bookingRulesVisible = true;
    $scope.earlyBird = false;
    $scope.lastMinute = false;
    $scope.dateBound = false;
    $scope.policies = {};
    $scope.policies.deposit = [];
    $scope.policies.cancellation = [];

    $scope.calendar = {};

    $scope.bookingRuleEdited = false;

	var cancIndex, depoIndex;
	var roomTypes = [];
	var deposit, cancellation;
	var allRooms = true;

	var ratePlanId = 0;

	 // download from database
 
    fetchDeposits();
    fetchCancellations();
    // import fake room json data
    fetchRooms();

    fetchRatePlans()
	

	$scope.roomRatePlan = function(per){
		$scope.ratePlanForm.per = per;
		if(per === "room"){
			$('#person').removeClass('clickedRooms');
			$('#room').addClass('clickedRooms');
		}
		else if(per === "person"){
			$('#room').removeClass('clickedRooms');
			$('#person').addClass('clickedRooms');
		}
	}

	$scope.assingRatePolicy = function(policy,index){
			if(policy === 'deposit'){
				$scope.ratePlanForm.deposit = $scope.policies.deposit[index];
				// assing index vaule to global var that allow to save deposit policy
				depoIndex = index;
				// change color of the clicked policy
				$('.depositPolicy').removeClass('clickedRooms');
				$('.depositPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(0).addClass('clickedRooms');
			}
			else if(policy === 'cancellation'){
				$scope.ratePlanForm.cancellation = $scope.policies.cancellation[index];
				// assing index vaule to global var that allow to save cancellation policy
				cancIndex = index;
				// change color of the clicked policy
				$('.cancellationPolicy').removeClass('clickedRooms');
				$('.cancellationPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(1).addClass('clickedRooms');
			}
			else if(policy === 'meal'){
				$('.mealPolicy').removeClass('clickedRooms');
				$('.mealPolicy').eq(index).addClass('clickedRooms');
				$('.dragAndDropBtn').eq(2).addClass('clickedRooms');
			}
			
	}

	$scope.chooseRoom = function(roomNum){
		
		var ifToRemove = false;

		if(roomNum == 'all'){
			roomTypes = [];
			if(allRooms)
			{
				$('#allRoomsBtn, .roomsBtn').addClass('clickedRooms');
				allRooms = !allRooms;
				for(var i in $scope.roomsData)
				{
					roomTypes.push(parseInt(i));
				}

			}
			else
			{
				$('#allRoomsBtn, .roomsBtn').removeClass('clickedRooms');
				allRooms = !allRooms;

			}
			
		}
		else{	
			if(!allRooms){allRooms = true};

			$('#allRoomsBtn').removeClass('clickedRooms');
			$('.roomsBtn').eq(roomNum).toggleClass('clickedRooms');

			for(var i = 0; i < roomTypes.length;i++){
				if(roomTypes[i] === roomNum){
					roomTypes.splice(i,1);
					// ifToRemove = true;
					return false;
				}
			}
			// if(!ifToRemove){roomTypes.push(roomNum)};
			roomTypes.push(roomNum);
			if(roomTypes.length == $scope.roomsData.length)
			{
				$('#allRoomsBtn, .roomsBtn').addClass('clickedRooms');
				allRooms = false;
			}
		}
	}

	$scope.saveRatePlan = function(){

		if(repeatedName($scope.ratePlanForm.name) && $scope.isEdited == 'false'){return alert('names cannot be repeated')};
		
		// validate and save edited 
		if($scope.isEdited != 'false'){
			if($scope.ratePlanForm.name.length == 0 || roomTypes.length == 0){return alert('please type your rate plan properly')};
			saveEdited();
			$scope.isEdited = 'false';
			return false;
		}

		var oneRatePlan = {};

		//assing user and hotel id
		oneRatePlan.hotelId = hotelId;
		oneRatePlan.userId = userId;

		// var currentTime = new Date();

		// oneRatePlan.created = currentTime;
		// oneRatePlan.updated = currentTime;
		oneRatePlan.name = $scope.ratePlanForm.name;
		oneRatePlan.per = $scope.ratePlanForm.per;
		oneRatePlan.meal = $scope.ratePlanForm.meal;
		if(depoIndex != null && cancIndex != null)
		{
		oneRatePlan.deposit = $scope.policies.deposit[depoIndex];
		oneRatePlan.cancellation = $scope.policies.cancellation[cancIndex];
		//assing additional variables in order to store cancellation/deposit object when edited
		deposit = oneRatePlan.deposit;
		cancellation = oneRatePlan.cancellation;
		}
		else
		{
			oneRatePlan.deposit = deposit;
			oneRatePlan.cancellation = cancellation;
		}

		//asign booking rules
		if($scope.ratePlanForm.bookingRule != 'undefined')
		{
			oneRatePlan.bookingRule = $scope.ratePlanForm.bookingRule;
		}
		else
		{
			oneRatePlan.bookingRule = false;
		}

		//roomTypes assigned in chooseRoom fucntion defined whether all types of rooms
		// or just some of them will be saved
			oneRatePlan.rooms = [];

			for(var i = 0; i < roomTypes.length; i++){
				var n = roomTypes[i];
				oneRatePlan.rooms.push($scope.roomsData[n]);
			}
		
		// check whether plan in inherited from another one and assing those values
		if($scope.ratePlanForm.inherit == true && $scope.ratePlanForm.inheritAmount >= 0 && $scope.ratePlanForm.inheritType !== undefined)
		{
			oneRatePlan.inherit = {};
			oneRatePlan.inherit.from = $scope.ratePlanForm.inheritRate.name;
			oneRatePlan.inherit.amount = $scope.ratePlanForm.inheritAmount;
			oneRatePlan.inherit.type = $scope.ratePlanForm.inheritType;
		}

		//validation
		if(!(validateRatePlan(oneRatePlan))){
			return alert('please type your rate plan properly');
		}
		//if validation succeed asing id to rate plan
		oneRatePlan.id = ++ratePlanId;

		roomTypes = [];

	
		//
		// send rate plan to database and save it in local system if succeed 
		//

		saveRatePlan(oneRatePlan);
		$scope.cleanForm();
	}

	$scope.cleanForm = function(){
		cancIndex = depoIndex = null;
		// change here
		deposit = cancellation = undefined;
		$scope.ratePlanForm.cancellation = $scope.ratePlanForm.deposit = $scope.ratePlanForm.meal = null;

	 	$scope.ratePlanForm.name = null
		$scope.ratePlanForm.per = null;
		$scope.ratePlanForm.rooms = null;
		//reset all marked colors
		$('#person, #room, .mealPolicy, .depositPolicy, .cancellationPolicy').removeClass('clickedRooms');
		$('.dragAndDropBtn').removeClass('clickedRooms');

		$scope.ratePlanVisible = false;
		$scope.ratePlanForm.yes = false;
		$scope.ratePlanForm.inherit = false;
		$scope.isEdited = 'false';
		$('#allRoomsBtn').removeClass('clickedRooms');
		$('.roomsBtn').removeClass('clickedRooms');
		$('#person').removeClass('clickedRooms');
		$('#room').removeClass('clickedRooms');

		// booking rules
		for(var i = 2; i < 5; i++){
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}
		// clean booking rules
		$scope.calendar = {};
		$scope.bookingRulesVisible = true;
		

		$window.scrollTo(0, 0);

	}

	$scope.findRatePlan = function(rateIndex){
		$scope.ratePlanToDelete = rateIndex;
	}

	$scope.deleteRatePlan = function(isEdited){
		
		if(isEdited != false){
			
			$scope.ratePlan.splice(isEdited,1);
			return true;
		}
		
		deleteFromDatabase($scope.ratePlan[$scope.ratePlanToDelete]);
		// if(deleteFromDatabase($scope.ratePlan[$scope.ratePlanToDelete]))
		// {
		// 	$scope.ratePlan.splice($scope.ratePlanToDelete,1);
		// 	$scope.ratePlanToDelete = null;
		// }
		// $scope.ratePlan.splice($scope.ratePlanToDelete,1);
		// $scope.ratePlanToDelete = null;
	}

	$scope.editRatePlan = function(rateIndex){

		$scope.isEdited = rateIndex;
		
		$scope.ratePlanVisible = true;
		$scope.ratePlanForm.firstName = $scope.ratePlan[rateIndex].name;
		$scope.ratePlanForm.name = $scope.ratePlan[rateIndex].name;
		$scope.ratePlanForm.per = $scope.ratePlan[rateIndex].per;
		// HERE
		$scope.ratePlanForm.deposit = $scope.ratePlan[rateIndex].deposit;
		$scope.ratePlanForm.cancellation = $scope.ratePlan[rateIndex].cancellation;
		$scope.ratePlanForm.meal = $scope.ratePlan[rateIndex].meal;
		
		//assing policies buttons to edit panel
		$('.depositPolicy:contains('+$scope.ratePlanForm.deposit.name+')').addClass('clickedRooms');
		$('.mealPolicy:contains('+$scope.ratePlanForm.meal+')').addClass('clickedRooms');
		$('.cancellationPolicy:contains('+$scope.ratePlanForm.cancellation.name+')').addClass('clickedRooms');

		$('.dragAndDropBtn').addClass('clickedRooms');

		// assing buttons to payment per room or per person panel
		if($scope.ratePlan[rateIndex].per == 'room')
		{
			$('#person').removeClass('clickedRooms');
			$('#room').addClass('clickedRooms');
		}
		else
		{
			$('#room').removeClass('clickedRooms');
			$('#person').addClass('clickedRooms');
		};

		
		//assign rooms buttons
			for(var i  = 0; i < Object.keys($scope.ratePlan[rateIndex].rooms).length; i++){
				var name = $scope.ratePlan[rateIndex].rooms[i].name;
				$('.roomsBtn:contains('+name+')').addClass('clickedRooms');

			}

			if($scope.ratePlan[rateIndex].rooms.length == $scope.roomsData.length){
				$('#allRoomsBtn').addClass('clickedRooms');
			}
			for(var i in $scope.roomsData){
				for(var n in $scope.ratePlan[rateIndex].rooms){
					if($scope.roomsData[i].name == $scope.ratePlan[rateIndex].rooms[n].name)
					{
						roomTypes.push(parseInt(i));
					}
				}
			}

			//booking rules
			if($scope.ratePlan[rateIndex].bookingRule != false && typeof($scope.ratePlan[rateIndex].bookingRule) != 'undefined')
			{
				$scope.bookingRulesVisible = false;

				// initialize bookingRule object in case of editing rate plan fetched from database
				$scope.ratePlanForm.bookingRule = {};

				$scope.ratePlanForm.bookingRule.name = $scope.ratePlan[rateIndex].bookingRule.name;
				if($scope.ratePlanForm.bookingRule.name == "Date Bound")
				{
					$scope.calendar.bookNow = $scope.ratePlan[rateIndex].bookingRule.bookDateFrom;
					$scope.calendar.bookTo = $scope.ratePlan[rateIndex].bookingRule.bookDateTo;
					$scope.calendar.now = $scope.ratePlan[rateIndex].bookingRule.stayDateFrom;
					$scope.calendar.to = $scope.ratePlan[rateIndex].bookingRule.stayDateTo;
				}
				else{
					$scope.calendar.days = $scope.ratePlan[rateIndex].bookingRule.days;
					$scope.calendar.now = $scope.ratePlan[rateIndex].bookingRule.activeFrom;
					$scope.calendar.to = $scope.ratePlan[rateIndex].bookingRule.activeTo;
				}

			}

	}

	
	$scope.inherit = function(inheritRate){
		$scope.ratePlanForm.inherit = true;
	}

	$scope.inheirtBtn = function(what)
	{
		if(what){
			$('.inherit').eq(0).addClass('clickedRooms');
			$('.inherit').eq(1).removeClass('clickedRooms');
		}
		else
		{
			$('.inherit').eq(1).addClass('clickedRooms');
			$('.inherit').eq(0).removeClass('clickedRooms');
		}
	}

	// booking rules



	$scope.bookingRules = function(index)
	{	
		for(var i = 2; i < 5; i++){
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}
		$('.centerButtons').eq(index+2).find('button').addClass('clickedRooms');
		
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;

		$scope.calendar.now = $scope.calendar.to = new Date();

		$scope.calendar.days = 0;

		switch(index){
			case 0:
			$scope.lastMinute = true;
			break;
			case 1:
			$scope.earlyBird = true;
			break;
			case 2:
			$scope.dateBound = true;
			$scope.calendar.bookNow = $scope.calendar.bookTo = new Date();
			break;
		}

	}

	$scope.addBookingRule = function(name)
	{	
		if($scope.calendar.now > $scope.calendar.to){return alert('please type your date properly')};

		$scope.ratePlanForm.bookingRule = {};
		$scope.ratePlanForm.bookingRule.name = name;

		if(name == "Date Bound")
		{	
			if($scope.calendar.bookNow > $scope.calendar.bookTo){return alert('please type your date properly')};
			
			$scope.ratePlanForm.bookingRule.stayDateFrom = $scope.calendar.now;
			$scope.ratePlanForm.bookingRule.stayDateTo =  $scope.calendar.to;
			$scope.ratePlanForm.bookingRule.bookDateFrom = $scope.calendar.bookNow;
			$scope.ratePlanForm.bookingRule.bookDateTo = $scope.calendar.bookTo;
		}
		else
		{	
			$scope.ratePlanForm.bookingRule.days = $scope.calendar.days;
			$scope.ratePlanForm.bookingRule.activeFrom = $scope.calendar.now;
			$scope.ratePlanForm.bookingRule.activeTo =   $scope.calendar.to;
		}

		//information for api functions
		if($scope.bookingRuleEdited)
		{
			$scope.ratePlanForm.bookingRule.instruction = "update";
			$scope.bookingRuleEdited = false;
		}
		else
		{
			$scope.ratePlanForm.bookingRule.instruction = "create";
		}

		$scope.bookingRulesVisible = false;
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;


		
	};

	$scope.cancelBookingRule = function()
	{
		$scope.earlyBird = $scope.lastMinute = $scope.dateBound = false;
		for(var i = 2; i < 5; i++){
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}
	};

	$scope.deleteBookingRule = function()
	{
		delete $scope.ratePlanForm.bookingRule;
		$scope.ratePlanForm.bookingRule = {};

		//information for api functions
		$scope.ratePlanForm.bookingRule.instruction = 'delete';
		
		for(var i = 2; i < 5; i++)
		{
			$('.centerButtons').eq(i).find('button').removeClass('clickedRooms');
		}

		$scope.bookingRulesVisible = true;
	};

	$scope.editBookingRule = function(name)
	{	
		
		$scope.bookingRulesVisible = true;

		$scope.bookingRuleEdited = true;

			switch(name){
			case 'Date Bound':
				
				$scope.dateBound = true;
				$('.centerButtons').eq(4).find('button').addClass('clickedRooms');
			break;
			case 'Early Bird':
				
				$scope.earlyBird = true;
				$('.centerButtons').eq(3).find('button').addClass('clickedRooms');
			break;
			case 'Last Minute':
				
				$scope.lastMinute = true;
				$('.centerButtons').eq(2).find('button').addClass('clickedRooms');
			break;
			};	

	};


	function saveEdited(){
		
		for(var i in $scope.ratePlan)
		{	

			if($scope.ratePlanForm.firstName == $scope.ratePlan[i].name)
			{	
				
				$scope.ratePlan[i].name = $scope.ratePlanForm.name;
				$scope.ratePlan[i].updated = new Date();
				$scope.ratePlan[i].per = $scope.ratePlanForm.per;
				$scope.ratePlan[i].deposit = $scope.ratePlanForm.deposit;
				$scope.ratePlan[i].cancellation = $scope.ratePlanForm.cancellation;
				$scope.ratePlan[i].meal = $scope.ratePlanForm.meal;
				$scope.ratePlan[i].userId = userId;
				$scope.ratePlan[i].rooms = [];
				
				//assing rooms to edited rate plan
				for(var n in roomTypes){
					var x = roomTypes[n];
					$scope.ratePlan[i].rooms.push($scope.roomsData[x]);
					
				}
				// reset rooms in form
				roomTypes = [];

				//asign booking rules
				$scope.ratePlan[i].bookingRule = {}
				$scope.ratePlan[i].bookingRule = $scope.ratePlanForm.bookingRule;
				
				$scope.cleanForm();
				return updateRatePlan($scope.ratePlan[i]);
			}
		}
		
	}

	function inheritanceOrder(inheritedRate){
		if($scope.ratePlan.length == 1){
			$scope.ratePlan.push(inheritedRate);
			return true;
		}
		
		for(var i = 0; i < $scope.ratePlan.length; i++)
		{
			if($scope.ratePlan[i].name === inheritedRate.inherit.from)
			{	
				$scope.ratePlan.splice(i+1,0,inheritedRate);
				return true;
			}	
		}
	}

	function repeatedName(name){
		for(var i = 0; i < $scope.ratePlan.length;i++){
			if($scope.ratePlan[i].name === name){
				return true;
			}
		}
		return false;
	}

	//validation 
	function validateRatePlan(ratePlan)
	{	
		
		if(typeof(ratePlan.name) == 'undefined' || ratePlan.name == null)
		{	
			return false;
		}
	
		if(ratePlan.name.length == 0)
		{
			return false;
		}
		if(typeof(ratePlan.meal) == 'undefined' || typeof(ratePlan.per) == 'undefined' || typeof(ratePlan.cancellation) == 'undefined' ||  typeof(ratePlan.deposit) == 'undefined')
		{	
			return false;
		}
		if(ratePlan.rooms.length == 0)
		{	
			return false;
		}
		
		return true;
	}

	// DATABASE FUNCTIONS

	function saveRatePlan(ratePlan)
	{
		$http.post('/databaseSend/ratePlan',ratePlan)
		.then(function(response){
			
			//if inherited change order of array to display inherited rate plan under its parent
	
			if(ratePlan.inherit)
			{	
				inheritanceOrder(ratePlan);
			}
			else
			{
				$scope.ratePlan.push(ratePlan);
			}

		})
		.catch(function(err){
			$('#databaseError').modal('show');
		});
	}

	function updateRatePlan(ratePlan)
	{
		$http.post('/databaseUpdate/ratePlan',ratePlan)
		.then(function(response){
			return true;
		})
		.catch(function(err){
			$('#databaseError').modal('show');
			return false;
		});
	}

	function fetchRatePlans()
	{
		$http.post('/user/ratePlans', {id: hotelId})
	    .then(function(response){
	    	var DbRatePlan = response.data;
	    	
	    	// assign deposits, booking rules, cancellations and rooms previously downloaded from database to each rate plan
	    	if(DbRatePlan.length > 0)
	    	{
	    	  for(var i in DbRatePlan)
	    	  {	
	    		$scope.ratePlan[i] = {};

	    		$scope.ratePlan[i].name = DbRatePlan[i].name;
	    		$scope.ratePlan[i].meal = DbRatePlan[i].meal;
	    		$scope.ratePlan[i].per = DbRatePlan[i].pricing;
	    		$scope.ratePlan[i].id = DbRatePlan[i].id;
	    		$scope.ratePlan[i].rooms = [];

	    		for(var n in $scope.policies.cancellation)
	    		{
	    			if($scope.policies.cancellation[n].id == DbRatePlan[i].cancellation)
	    			{
	    				$scope.ratePlan[i].cancellation = $scope.policies.cancellation[n];
	    			}
	    		} 

	    		for(var x in $scope.policies.deposit)
	    		{
	    			if($scope.policies.deposit[x].id == DbRatePlan[i].deposit)
	    			{
	    				$scope.ratePlan[i].deposit = $scope.policies.deposit[x];	
	    			}
	    		}

	    		for(var y in DbRatePlan[i].rooms)
	    		{	
	   				var id = DbRatePlan[i].rooms[y].room_id;
	    			$scope.ratePlan[i].rooms.push($scope.roomsData[id-1]);

	    		}

	    		// booking rules
	    		if(DbRatePlan[i].hasOwnProperty("bookingRule"))
	    		{	
	    			$scope.ratePlan[i].bookingRule = {}

	    			if(DbRatePlan[i].bookingRule.type == "Date Bound")
	    			{
	    				$scope.ratePlan[i].bookingRule.name = DbRatePlan[i].bookingRule.type;
	    				$scope.ratePlan[i].bookingRule.stayDateFrom = new Date(DbRatePlan[i].bookingRule.date_from_one);
	    				$scope.ratePlan[i].bookingRule.stayDateTo =  new Date(DbRatePlan[i].bookingRule.date_to_one);
	    				$scope.ratePlan[i].bookingRule.bookDateFrom = new Date(DbRatePlan[i].bookingRule.date_from_two);
	    				$scope.ratePlan[i].bookingRule.bookDateTo = new Date(DbRatePlan[i].bookingRule.date_to_two);
	    				
	    			}
	    			else
	    			{	
	    				$scope.ratePlan[i].bookingRule.name = DbRatePlan[i].bookingRule.type;
	    				$scope.ratePlan[i].bookingRule.days = DbRatePlan[i].bookingRule.days;
	    				$scope.ratePlan[i].bookingRule.activeFrom = new Date(DbRatePlan[i].bookingRule.date_from_one);
	    				$scope.ratePlan[i].bookingRule.activeTo = new Date(DbRatePlan[i].bookingRule.date_to_one);
	    			}
	    			
	    		}	
	    	  }

	    	}

	    })
	    .catch(function(err){
	    	if(err){$('#databaseError').modal('show')};
	    });
	}

	function fetchDeposits()
	{
	    $http.post('/user/deposits',{id: hotelId})
	    .then(function(response){
	    	  for(var i in response.data)
	                {   
	                    $scope.policies.deposit[i] = {};
	                    $scope.policies.deposit[i].name = response.data[i].name;
	                    $scope.policies.deposit[i].hotelId = response.data[i].hotel_id;
	                    $scope.policies.deposit[i].userId = response.data[i].user_id;
	                    $scope.policies.deposit[i].id = response.data[i].id;
	                    $scope.policies.deposit[i].amount = response.data[i].deposit;
	                    $scope.policies.deposit[i].additionalPayment = {} 
	                    $scope.policies.deposit[i].additionalPayment.amount = response.data[i].add_amount;
	                    $scope.policies.deposit[i].additionalPayment.daysBefore = response.data[i].add_days;
	                    $scope.policies.deposit[i].additionalPayment1 = {} 
	                    $scope.policies.deposit[i].additionalPayment1.amount = response.data[i].add_amount1;
	                    $scope.policies.deposit[i].additionalPayment1.daysBefore = response.data[i].add_days1;
	                }
	    }).catch(function(err){$('#databaseError').modal('show')});
	}

	function fetchCancellations()
	{
	     $http.post('/user/cancellations',{id: hotelId})
	    .then(function(response){
	    	 for(var i in response.data)
	                {   
	                    $scope.policies.cancellation[i] = {};
	                    $scope.policies.cancellation[i].name = response.data[i].name;
	                    $scope.policies.cancellation[i].hotelId = response.data[i].hotel_id;
	                    $scope.policies.cancellation[i].id = response.data[i].id;
	                }
	    })
	    .catch(function(err){$('#databaseError').modal('show')});
	}

	function fetchRooms()
	{
		$http.get('rooms.json').then(function(res){
		$scope.roomsData = res.data;
		})
		.catch(function(err){
			if(err){$('#databaseError').modal('show')}
		});
	}

	function deleteFromDatabase(ratePlan)
	{
		$http.post('/databaseDelete/ratePlan',ratePlan)
		.then(function(response){
			// delete from local system
			$scope.ratePlan.splice($scope.ratePlanToDelete,1);
		 	$scope.ratePlanToDelete = null;
		})
		.catch(function(err){
			if(err){$('#databaseError').modal('show')}
		});
	}

}]);

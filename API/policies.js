var fs = require('fs');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require('express');
var config = require('../config.json');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(typeof config.database.ssl != 'undefined') {
	config.database.ssl.ca = fs.readFileSync(config.database.ssl.caPath);
	config.database.ssl.cert = fs.readFileSync(config.database.ssl.certPath);
	config.database.ssl.key = fs.readFileSync(config.database.ssl.keyPath);	
}

var connection = mysql.createConnection(config.database);

connection.connect(function(err){
	if(err){
		fs.writeFileSync('error.log',new Date() + ' database connection error \n',{'flag': 'a'});
		fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
		return console.log(err);
	}
});


module.exports = 
{

	fetchDeposit: function(req,res){
		
		connection.query("SELECT * FROM cmg_deposits WHERE hotel_id = ?", req.body.id, function(err,results){
			if(err){
			fs.writeFileSync('error.log',new Date() + ' API error - /user/deposits \n',{'flag': 'a'});
			fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
			return res.sendStatus(500)};

			if(results.length > 0)
			{
				res.send(results);
			}
			else{
				res.send('not found');
			}
		});	
},



	fetchCancellation:  function(req,res){
		var data = {};
		connection.query("SELECT * FROM cmg_cancellations WHERE hotel_id = ?", req.body.id, function(err,results){
		if(err){
			fs.writeFileSync('error.log',new Date() + ' API error - /user/cancellations \n',{'flag': 'a'});
			fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
			return res.sendStatus(500);}
		else if(results.length > 0)
		{	
			var cancellations = results;
			var cancellationID = [];
			var query =  "SELECT * FROM cmg_rules_cancellation WHERE cancellation_id = ? ";
			for(var i in results)
			{	
				// find ID of the cancellation record in order to select cancellation rules selected to them
				cancellationID.push(results[i].id);
				if(i > 0)
				{
					query += " OR cancellation_id = ?";
				}
			}
			
			// fetch cancellation rules
			connection.query(query, cancellationID,
				function(err,rules){
					if(err){
						fs.writeFileSync('error.log',new Date() + ' API error - /user/cancellations \n',{'flag': 'a'});
						fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
						return res.sendStatus(500);}
					else
					{
						data.rules = rules;
						data.cancellations = cancellations;
						res.send(data);
					}
				});

		}


		else
		{
			res.send('not found');
		}
	});	

},

	

	fetchChildren: function(req,res){	
	var data;
	connection.query("SELECT * FROM cmg_children WHERE hotel_id = ?", req.body.id, function(err,results){
		if(err){
			fs.writeFileSync('error.log',new Date() + ' API error - /user/children \n',{'flag': 'a'});
			fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
			return res.sendStatus(500)};

		if(results.length > 0)
		{
			res.send(results);
		}
		else{
			res.send('not found');
		}
	});	

},

	sendDeposit: function(req,res){
	var depositReceived = req.body;


		if(typeof(depositReceived.additionalPayment) === 'undefined')
		{
			depositReceived.additionalPayment = {};
			depositReceived.additionalPayment.amount = null; 
			depositReceived.additionalPayment.daysBefore = null;
		};
		if(typeof(depositReceived.additionalPayment1) === 'undefined')
		{
			depositReceived.additionalPayment1 = {};
			depositReceived.additionalPayment1.amount = null; 
			depositReceived.additionalPayment1.daysBefore = null;
		};

	var depositToSend = []
		depositToSend[0] = 
			[	
				 depositReceived.name,
				 depositReceived.amount,
				 depositReceived.additionalPayment.amount,
				 depositReceived.additionalPayment.daysBefore,
				 depositReceived.additionalPayment1.amount,
				 depositReceived.additionalPayment1.daysBefore,
				 depositReceived.hotel_id,
				 depositReceived.user_id

			];
	

	connection.query("INSERT INTO cmg_deposits (name, deposit, add_amount, add_days, add_amount1, add_days1, hotel_id, user_id) VALUES ?", [depositToSend], function(err,results){
		if(err){
			fs.writeFileSync('error.log',new Date() + ' API error - /databaseSend/deposit \n',{'flag': 'a'});
			fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
			return res.sendStatus(500)}
		else
		{
			res.sendStatus(200);
		}

	});
},

	sendCancellation: function(req,res){
		var cancellationReceived = req.body;
		var cancellationID;
		var rules = [];
		var cancellation = [];
		cancellation[0] = 
		[
			cancellationReceived.name,
			cancellationReceived.hotel_id,
			cancellationReceived.user_id
		]

		connection.query('INSERT INTO cmg_cancellations (name, hotel_id, user_id) values ?', [cancellation], function(err, results) {
	    if (err){
	       fs.writeFileSync('error.log',new Date() + ' API error - /databaseSend/cancellation \n',{'flag': 'a'});
		   fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
	       return res.sendStatus(500);
	    }

	     var cancellation_id = results.insertId;

	    for(var i = 0; i < cancellationReceived.rule.length; i++)
		{
			rules[i] = 
			[
				cancellationReceived.rule[i].daysBefore,
				cancellationReceived.rule[i].fee,
				cancellationReceived.rule[i].type,
				cancellation_id
			]
		}
			
			 connection.query('INSERT INTO cmg_rules_cancellation (period, amount, type, cancellation_id) values ?', [rules], 
			 function(err,results){
			 	if(err){
				 	fs.writeFileSync('error.log',new Date() + ' API error - /databaseSend/deposit \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
				 	return res.sendStatus(500)}
			 	else
			 	{
			 		res.sendStatus(200);
			 	}
			 });	

		});

},

	sendChildren: function(req,res){
	
		var chidren = req.body;
		var childrenPolicyToSend = [];
		childrenPolicyToSend[0] = 
		[
		chidren.name, 
		chidren.ageRange, 
		chidren.hotel_id,
		chidren.user_id
		]

		connection.query("INSERT INTO cmg_children (type, age_range, hotel_id, user_id) values ?", [childrenPolicyToSend],
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseSend/children \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{
					res.sendStatus(200);
				}
			});
},

	deleteDeposit: function(req,res){
		
		var depositReceived = req.body.deposit;

		var ratePlansReceived = req.body.ratePlan;


		// if deposit policy is related to any rate plans delete it with rooms, booking rules and rates
		if(ratePlansReceived.id.length > 0)
		{	
			var ratePlanIDs = [];
			var ratePlans = [];

			for(var i in ratePlansReceived.id)
			{	
				ratePlanIDs.push(ratePlansReceived.id[i]);
				ratePlans[i] = [];
				ratePlans[i].push(ratePlansReceived.name[i]);
				ratePlans[i].push(ratePlansReceived.hotel_id);
			};

			
			
			connection.query("DELETE FROM cmg_rate_plans WHERE id = ?", ratePlanIDs,function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{	
					connection.query("DELETE FROM cmg_rate_plans_rooms WHERE rate_plan_id = ?", ratePlanIDs, function(err,results){
						if(err){
							fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
							fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
							return res.sendStatus(500)}
						else
						{	
								
							connection.query("DELETE FROM cmg_rates WHERE (rate_plan, hotel_id) IN (?)", [ratePlans],
								function(err,results){
									if(err){
										fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
										fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
										return res.sendStatus(500)}
									else
									{	
										connection.query("DELETE FROM cmg_booking_rules WHERE rate_plan_id = ?", ratePlanIDs, function(err,results){
											if(err){
												fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
												fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
												return res.sendStatus(500)}
											else
											{	
												connection.query("DELETE FROM cmg_deposits WHERE hotel_id = ? AND NAME = ?", [depositReceived.hotel_id, depositReceived.name], function(err,results){
													if(err){
														fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
														fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
														return res.sendStatus(500)}
													else
													{
														res.sendStatus(200);
													}
												});
											}
										});
									};
								});	
						}

					});
				}
			});
		}

		// if not just delete deposit itself
		else
		{	
			connection.query("DELETE FROM cmg_deposits WHERE hotel_id = ? AND NAME = ?", [depositReceived.hotel_id, depositReceived.name], function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{
					res.sendStatus(200);
				}
			});	

		};
			
		
},

    deleteCancellation: function(req,res){
	
	
		var cancellationReceived = req.body.cancellation;
		var ratePlansReceived = req.body.ratePlan;

		var cancellationDeleted = 
		{
				name: cancellationReceived.name,
				hotel_id: cancellationReceived.hotel_id
		};
		
		
		// if any related rate plans 
		if(ratePlansReceived.id.length > 0)
		{	
			var ratePlanIDs = [];
			var ratePlans = [];

			for(var i in ratePlansReceived.id)
			{	
				ratePlanIDs.push(ratePlansReceived.id[i]);
				ratePlans[i] = [];
				ratePlans[i].push(ratePlansReceived.name[i]);
				ratePlans[i].push(ratePlansReceived.hotel_id);
			};

			
			
			connection.query("DELETE FROM cmg_rate_plans WHERE id = ?", ratePlanIDs,function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{	
					connection.query("DELETE FROM cmg_rate_plans_rooms WHERE rate_plan_id = ?", ratePlanIDs, function(err,results){
						if(err){
							fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
							fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
							return res.sendStatus(500)}
						else
						{	
								
							connection.query("DELETE FROM cmg_rates WHERE (rate_plan, hotel_id) IN (?)", [ratePlans],
								function(err,results){
									if(err){
										fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
										fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
										return res.sendStatus(500)}
									else
									{	
										connection.query("DELETE FROM cmg_booking_rules WHERE rate_plan_id = ?", ratePlanIDs, function(err,results){
											if(err){
												fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
												fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
												return res.sendStatus(500)}
											else
											{	
												connection.query("DELETE FROM cmg_rules_cancellation WHERE cancellation_id = (SELECT id FROM cmg_cancellations WHERE hotel_id = ? AND name = ?)", [cancellationDeleted.hotel_id, cancellationDeleted.name],
													function(err,results){
														if(err){
															fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
															fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
															return res.sendStatus(500)}
													});

													connection.query("DELETE FROM cmg_cancellations WHERE hotel_id = ? AND name = ?",[cancellationDeleted.hotel_id, cancellationDeleted.name],
														function(err,results){
															if(err){
																fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellation \n',{'flag': 'a'});
																fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
																return res.sendStatus(500)}
															else
															{
																res.sendStatus(200);
															};
															
														});
											}
										});
									};
								});	
						}

					});
				}
			});
		}

		// without any related rate plans 
		else
		{
			connection.query("DELETE FROM cmg_rules_cancellation WHERE cancellation_id = (SELECT id FROM cmg_cancellations WHERE hotel_id = ? AND name = ?)", [cancellationDeleted.hotel_id, cancellationDeleted.name],
				function(err,results){
					if(err){
						fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
						fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
						return res.sendStatus(500)}
				});

			connection.query("DELETE FROM cmg_cancellations WHERE hotel_id = ? AND name = ?",[cancellationDeleted.hotel_id, cancellationDeleted.name],
				function(err,results){
					if(err){
						fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/deposit \n',{'flag': 'a'});
						fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
						return res.sendStatus(500)}
							else
							{
								res.sendStatus(200);
							};
														
				});
	
		};

},

	deleteChildren: function(req,res){
		var children = req.body;
		var childrenPolicyToDelete = [];
		
		connection.query("DELETE FROM cmg_children WHERE type = ? AND hotel_id = ? ", [children.name, children.hotel_id] ,
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/children \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{
					res.sendStatus(200);
				}
			});

},
		
	updateDeposit: function(req,res){

		var depositReceived = req.body;
		var recordID;

		if(typeof(depositReceived.additionalPayment) === 'undefined')
			{	
				depositReceived.additionalPayment = {};
				depositReceived.additionalPayment.amount = null; 
				depositReceived.additionalPayment.daysBefore = null;
			};
		if(typeof(depositReceived.additionalPayment1) === 'undefined')
			{	
				depositReceived.additionalPayment1 = {};
				depositReceived.additionalPayment1.amount = null; 
				depositReceived.additionalPayment1.daysBefore = null;
			};

		var depositUpdated = 
		{
		 	name:            depositReceived.name,
			deposit:	     depositReceived.amount,
			add_amount:	 	 depositReceived.additionalPayment.amount,
			add_days:	     depositReceived.additionalPayment.daysBefore,
			add_amount1:	 depositReceived.additionalPayment1.amount,
			add_days1:	     depositReceived.additionalPayment1.daysBefore,
			user_id:  		 depositReceived.user_id,
			hotel_id:	     depositReceived.hotel_id,
			oldName: 		 depositReceived.oldName
		};
		

		connection.query("UPDATE cmg_deposits SET name = ?, deposit = ?, add_amount = ?, add_days = ?, add_amount1 = ?, add_days1 = ?, user_id = ? WHERE hotel_id = ? AND name = ? ",
	    [depositUpdated.name, depositUpdated.deposit, depositUpdated.add_amount, depositUpdated.add_days, depositUpdated.add_amount1, depositUpdated.add_days1, depositUpdated.user_id, depositUpdated.hotel_id, depositUpdated.oldName],
			function(err, results){
			 if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseUpdate/deposit \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
			 else
			 {
			 	res.sendStatus(200);
			 }
			
		});
	
},

	updateCancellation: function(req,res){
	var cancellation = req.body;
	
	connection.query("SELECT ID FROM cmg_cancellations WHERE name = ? AND hotel_id = ?; UPDATE cmg_cancellations SET name = ?, user_id = ? WHERE hotel_id = ? AND name =?", [cancellation.oldName, cancellation.hotel_id, cancellation.name, cancellation.user_id, cancellation.hotel_id, cancellation.oldName],
		function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseUpdate/cancellation \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
			else
			{	

				var cancellationID = JSON.stringify(results[0][0].ID);


				var queryUpdate = 'SET foreign_key_checks = 0;';
				var queryInsert = 'INSERT INTO cmg_rules_cancellation (period, amount, type, cancellation_id) values ? ';
				var dataUpdate = [];
				var dataInsert = [];
				for(var i in cancellation.rule)
				{	

					if(typeof(cancellation.rule[i].id) != "undefined")
					{
						queryUpdate += "UPDATE cmg_rules_cancellation SET period = ?, amount = ?, type = ? WHERE id = ?; ";
						dataUpdate.push(cancellation.rule[i].daysBefore);
						dataUpdate.push(cancellation.rule[i].fee);
						dataUpdate.push(cancellation.rule[i].type);
						dataUpdate.push(cancellation.rule[i].id);
					}
					else
					{	
						var temp = 
						[
						cancellation.rule[i].daysBefore,
						cancellation.rule[i].fee,
						cancellation.rule[i].type,
						cancellationID
						];
						dataInsert.push(temp);
					}


				}
				
					connection.query(queryUpdate, dataUpdate, function(err,results){
							if(err){
								fs.writeFileSync('error.log',new Date() + ' API error - /databaseUpdate/cancellation \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								return res.sendStatus(500)}
							else if (dataInsert.length > 0)
							{

								connection.query(queryInsert, [dataInsert],function(err,results){
									if(err){
										fs.writeFileSync('error.log',new Date() + ' API error - /databaseUpdate/cancellation \n',{'flag': 'a'});
										fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
										return res.sendStatus(500)}
									else
									{
										return res.sendStatus(200);
									}
								});

							}
							else
							{
								res.sendStatus(200);
							}
					});

			}
		});

},

	updateChildren: function(req,res){
		var children = req.body;
		var childrenPolicyToUpdate = 
		{
			type: children.name,
			age_range: children.ageRange,
			hotel_id: children.hotel_id,
			old_name: children.oldName
		};

		connection.query("UPDATE cmg_children set type = ?, age_range = ? WHERE hotel_id = ? AND type = ?",
		[childrenPolicyToUpdate.type, childrenPolicyToUpdate.age_range, childrenPolicyToUpdate.hotel_id, childrenPolicyToUpdate.old_name],
		function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseUpdate/children \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
			else
			{
				res.sendStatus(200);
			}
		});

},

	findRelatedToDeposit: function(req,res){
		
		var depositReceived = req.body;
	
		var depositDeleted = 
		{
			name: depositReceived.name,
			hotel_id: depositReceived.hotel_id
		}

		connection.query("SELECT name,id FROM cmg_rate_plans WHERE deposit = (SELECT id FROM cmg_deposits WHERE hotel_id = ? AND name = ?)",[depositDeleted.hotel_id, depositDeleted.name],
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/depositFindRelated \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{
					res.send(results);
				}
			});
},

	findRelatedToCancellation: function(req,res){

		var cancellationReceived = req.body;

		var cancellationDeleted = 
		{
			name: cancellationReceived.name,
			hotel_id: cancellationReceived.hotel_id
		}

		connection.query("SELECT name,id FROM cmg_rate_plans WHERE cancellation = (SELECT id FROM cmg_cancellations WHERE hotel_id = ? AND name = ?)",[cancellationDeleted.hotel_id, cancellationDeleted.name],
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error - /databaseDelete/cancellationFindRelated \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500)}
				else
				{	
					res.send(results);
				}
			});

}

}
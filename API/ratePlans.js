var fs = require('fs');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require('express');

var config = require('../config.json');
var cmg = require('./cmg.js');

var app = express();

var request = require('request');
var Promise = require('promise');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(typeof config.database.ssl != "undefined") {
	config.database.ssl.ca = fs.readFileSync(config.database.ssl.caPath);
	config.database.ssl.cert = fs.readFileSync(config.database.ssl.certPath);
	config.database.ssl.key = fs.readFileSync(config.database.ssl.keyPath);	
}

var connection = mysql.createConnection(config.database);

connection.connect(function(err){
	if(err){
		fs.writeFileSync('error.log',new Date() + ' database connection error \n',{'flag': 'a'});
		fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
		return console.log(err);
	}
});


module.exports = 
{

	deposits: function(req,res){
		
		connection.query('SELECT * FROM cmg_deposits WHERE hotel_id = ?', req.body.id,function(err,results){
			if(err){
				fs.writeFileSync('error.log',new Date() + ' API error /user/deposits \n',{'flag': 'a'});
				fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
				return res.sendStatus(500);
			}
			
			res.send(results);
		});

},


	cancellations: function(req,res){
	
		connection.query('SELECT * FROM cmg_cancellations WHERE hotel_id = ?', req.body.id,function(err,results){
			if(err){fs.writeFileSync('error.log',new Date() + ' API error /user/deposits \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
			}
			res.send(results);
		});
},



	fetchRatePlans: function(req,res){
		var id = req.body.id;

		var ratePlanId = [];

		connection.query("SELECT * FROM cmg_rate_plans WHERE hotel_id = ? ", id, function(err,results){
			if(err){fs.writeFileSync('error.log',new Date() + ' API error /user/ratePlans \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
			}
			else
			{
				var ratePlan = results;
				
				var query = "SELECT * FROM cmg_rate_plans_rooms WHERE rate_plan_id = ? ";
				for(var i in ratePlan)
				{
					ratePlanId.push(ratePlan[i].id);
					if(i > 0)
					{
						query += "OR rate_plan_id = ? ";
					}

				}


				// fetch rooms related to rate plans if any exist

				if(ratePlanId.length > 0){
						connection.query(query, ratePlanId ,function(err,roomResults){
							if(err){fs.writeFileSync('error.log',new Date() + ' API error /user/ratePlans \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								return res.sendStatus(500);
							}
							else
							{
								
								// fetch booking rules and assign them to rate plans if exist
								connection.query("SELECT * FROM cmg_booking_rules WHERE rate_plan_id IN (?)", [ratePlanId] , function(err,bookingRuleResults){
									if(err){fs.writeFileSync('error.log',new Date() + ' API error /user/ratePlans \n',{'flag': 'a'});
											fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
											return res.sendStatus(500);
									}
									else
									{
										for(var i in ratePlan)
										{	
											ratePlan[i].rooms = [];

											for(var n in roomResults)
											{	

												if(ratePlan[i].id == roomResults[n].rate_plan_id)
												{
												  ratePlan[i].rooms.push(roomResults[n]);
												}
											}

											// check if rate plan id matching to booking rule
											for(var x in bookingRuleResults)
											{
												if(ratePlan[i].id == bookingRuleResults[x].rate_plan_id)
												{
													ratePlan[i].bookingRule = bookingRuleResults[x]
												}
											}
											
									    }

									res.send(ratePlan);

									}
								});

								
							}
						});
				}
				// send empty response if no rate plans exists 
				else
				{
					res.send(null);
				}
			}
		});

},


	sendRatePlan: function(req,res){
		
		var ratePlan = req.body.ratePlan;
		var uid = req.body.uid;

		var roomsId = [];
		var ratePlanToSend = []
		var bookingRule = [];

		var tempDerrived = {};

		if(ratePlan.hasOwnProperty('inherit')){
			tempDerrived.from = ratePlan.inherit.from;
			tempDerrived.type = ratePlan.inherit.type;
			tempDerrived.amount = ratePlan.inherit.amount;
		}
		else
		{
			tempDerrived.from = tempDerrived.type = tempDerrived.amount = null;
		}

		ratePlanToSend[0] = 
		[
			ratePlan.name,
			ratePlan.meal,
			ratePlan.cancellation.id,
			ratePlan.deposit.id,
			ratePlan.per,
			tempDerrived.from,
			tempDerrived.type,
			tempDerrived.amount,
			ratePlan.userId,
			ratePlan.hotelId
		];
		

		// preparing booking rule data to send if it exists 
		if(ratePlan.hasOwnProperty("bookingRule"))
		{
			if(ratePlan.bookingRule.name == "Date Bound")
			{
				bookingRule[0] = 
				[
					ratePlan.bookingRule.name,
					null,
					ratePlan.bookingRule.stayDateFrom,
					ratePlan.bookingRule.stayDateTo,
					ratePlan.bookingRule.bookDateFrom,
					ratePlan.bookingRule.bookDateTo

				]
			}
			else
			{
				bookingRule[0] = 
				[
					ratePlan.bookingRule.name,
					ratePlan.bookingRule.days,
					ratePlan.bookingRule.activeFrom,
					ratePlan.bookingRule.activeTo,
					null,
					null
				]
			}
		};

		
		// prepare options for cmg sending
		var ratePlanOptions = cmg.ratePlanCreate(ratePlan.name, uid);
		
		// save rate plan in cmg
		request(ratePlanOptions, function(err, response, body){	
			
			if (!err && response.statusCode == 201)
			{
			 	  
			    var cmgRatePlanID = body.id;
			    ratePlanToSend[0].push(cmgRatePlanID);

			    //send rate plan in local database
			    connection.query("INSERT INTO cmg_rate_plans(name,meal,cancellation,deposit,pricing,derived_rate_plan,derived_type,derived_amount,user_id,hotel_id,cmg_id) VALUES ?",[ratePlanToSend],
				function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/ratePlan \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{	
					
					var ratePlanId = results.insertId;
					var productFunctions = [];
					var productCmgIds = [];

					for(var i in ratePlan.rooms)
					{	
						var temp = [];
						temp.push(ratePlan.rooms[i].id);
						temp.push(ratePlanId);
						roomsId.push(temp);
					};

					for(var n in ratePlan.rooms)
					{
						var options = cmg.productCreate(uid,cmgRatePlanID,ratePlan.rooms[n].cmgId);
						
						// create product request for cmg
						productFunctions.push(new Promise(function(resolve,reject){
							request(options,function(err,response,body){
								if(!err)
								{
									resolve(body.id);
								}
								else
								{
									reject(err);
								}
							});
						}));


						
					};

						// send all request for products to cmg system
						Promise.all(productFunctions)
						.then(function(response){
								
								if(typeof(response) === 'object')
								{
									response = response.sort(function(a,b){return a-b});
								}

								// assign product ids from cmg to arrays which will be sended to local db
								for(var i in roomsId)
								{
									roomsId[i].push(response[i]);	
								};
									
								
								connection.query("INSERT INTO cmg_rate_plans_rooms (room_id, rate_plan_id, cmg_id) VALUES ?", [roomsId],
								function(err,results){
									if(err){
										fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/ratePlan \n',{'flag': 'a'});
										fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
										return res.sendStatus(500);
									}
									else
									{	
										var dataToSend = {
											id: ratePlanId,
											cmgId: cmgRatePlanID
										};
										
										// insert booking rule related to rate plan if exits 
										if(ratePlan.hasOwnProperty("bookingRule"))
										{	
											bookingRule[0].unshift(ratePlanId);

											connection.query("INSERT INTO cmg_booking_rules (rate_plan_id, type, days, date_from_one, date_to_one, date_from_two, date_to_two) VALUES ?",[bookingRule],
												function(err,results){
													if(err){fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/ratePlan \n',{'flag': 'a'});
														fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
														return res.sendStatus(500);
													}
													else
													{	
														res.send(dataToSend);
													}
												});
										}

										// if not, just send back response to client side app
										else
										{	
											res.send(dataToSend);
										}
									}
								});
						});
					
						
						};
					});


			}

			else
			{
				fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/ratePlan - CMG\n',{'flag': 'a'});
				fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
				return res.sendStatus(500);
			};
		});

},
	sendProduct: function(req,res){
		
		// setTimeout(function(){

		// 	var data = req.body;

		// 	var options = cmg.fetchProducts(data.cmgId);

		// 	rp(options).then(function(resp){

		// 		var productIds = JSON.parse(resp);

		// 		var updated = [];
				
		// 		for(var i in data.rooms)
		// 		{
		// 			updated[i] = [];
		// 			updated[i].push(productIds._embedded.products[i].id);
		// 			updated[i].push(data.id);
		// 			updated[i].push(data.rooms[i].id);
		// 		};

		// 		for(var n in updated)
		// 		{
		// 			connection.query("UPDATE cmg_rate_plans_rooms SET cmg_id = ? WHERE rate_plan_id = ? AND room_id = ?", updated[n],
		// 				function(err,response){
		// 					if(err){return console.log(err)};
		// 				});
		// 		};

		// 		res.sendStatus(200);
		// },40);

	 //   });

	},

	updateRatePlan: function(req,res){

		var ratePlanUpdate = req.body.ratePlan;
		var propertyCmgID = req.body.propertyCmgId;

	
		var roomsId = [];
		var roomsIdFromDB = [];

		var bookingRule = [];

		if(ratePlanUpdate.hasOwnProperty("bookingRule"))
		{	
			
			if(ratePlanUpdate.bookingRule.name == "Date Bound")
			{
				bookingRule[0] = 
				[
					ratePlanUpdate.bookingRule.name,
					null,
					ratePlanUpdate.bookingRule.stayDateFrom,
					ratePlanUpdate.bookingRule.stayDateTo,
					ratePlanUpdate.bookingRule.bookDateFrom,
					ratePlanUpdate.bookingRule.bookDateTo

				]
			}
			else
			{
				bookingRule[0] = 
				[
					ratePlanUpdate.bookingRule.name,
					ratePlanUpdate.bookingRule.days,
					ratePlanUpdate.bookingRule.activeFrom,
					ratePlanUpdate.bookingRule.activeTo,
					null,
					null
				]
			}
		}

		
		
		for(var i in ratePlanUpdate.rooms)
		{
			roomsId.push(parseInt(ratePlanUpdate.rooms[i].id));
		}
		
		var ratePlanOptions = cmg.ratePlanUpdate(ratePlanUpdate.cmgId, ratePlanUpdate.name);
		
		request(ratePlanOptions,function(err,response,body){
			
			if(!err && response.statusCode == 200){
				
				connection.query("UPDATE cmg_rate_plans SET name = ?, meal = ?, cancellation = ?, deposit = ?, pricing = ?, user_id = ? WHERE id = ?",
				[ratePlanUpdate.name, ratePlanUpdate.meal, ratePlanUpdate.cancellation.id, ratePlanUpdate.deposit.id, ratePlanUpdate.per, ratePlanUpdate.userId, ratePlanUpdate.id],
				function(err,results){
					if(err){
						fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
						fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
						return res.sendStatus(500);
					}
					else
					{	
						connection.query("SELECT room_id, cmg_id FROM cmg_rate_plans_rooms WHERE rate_plan_id = ?", ratePlanUpdate.id, function(err,results){
							if(err){
								fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								return res.sendStatus(500);
							}
							else
							{	
								
								for(var i in results)
								{
									roomsIdFromDB.push(results[i].room_id);
								};
								//compare arrays of rooms and send a query to update relation in database

									var diffOne = roomsId.filter(x => roomsIdFromDB.indexOf(x) < 0);
									var diffTwo = roomsIdFromDB.filter(x => roomsId.indexOf(x) < 0);
							
									var insertRooms = [];
									var deleteRooms = [];
									
									for(var i in diffOne)
									{	
										insertRooms[i] = [];
										insertRooms[i].push(diffOne[i]);
										insertRooms[i].push(ratePlanUpdate.id);
									};

									for(var i in diffTwo)
									{
										deleteRooms[i] = [];
										deleteRooms[i].push(diffTwo[i]);
										deleteRooms[i].push(ratePlanUpdate.id);
									};

								
									// delete products choosed by user both from cmg and local db
									if(diffTwo.length > 0)
									{	
											for(var i in deleteRooms)
											{
												for(var n in results)
												{	

													if(deleteRooms[i][0] == results[n].room_id){
														deleteRooms[i].push(results[n].cmg_id);
														break;
													};
												};
											};
											
											var deleteRequests = [];
											
											//prepare delete request for cmg products
											for(var i in deleteRooms){

												var options = cmg.deleteProduct(deleteRooms[i][2]);
												
												deleteRequests.push(new Promise(function(resolve,reject){
													request(options,function(err,response,body){
														
														if(!err){
															 resolve();
														}
														else
														{
															 reject(err);
														}
													});
												}));
											};

											Promise.all(deleteRequests)
											.then(function(response){

												connection.query("DELETE FROM cmg_rate_plans_rooms WHERE (room_id, rate_plan_id, cmg_id) IN (?)",[deleteRooms],
												function(err,results){
														if(err){
															fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
															fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
															return res.sendStatus(500);
														}
														else
														{	
															
														}
													});
											});

										
									};

									// insert new products to cmg and local db
									if(insertRooms.length > 0)
									{	
										var insertRequests = [];
										var roomsCmgID = []

										for(var i in insertRooms)
										{
											for(var n in ratePlanUpdate.rooms)
											{	
												
												if(insertRooms[i][0] == ratePlanUpdate.rooms[n].id)
												{
													roomsCmgID.push(ratePlanUpdate.rooms[n].cmgId);
													break;
												};
											};
										};
										
										
										for(var i in insertRooms)
										{
											var options = cmg.productCreate(propertyCmgID,ratePlanUpdate.cmgId,roomsCmgID[i])
											
											insertRequests.push(new Promise(function(resolve,reject){
												request(options,function(err,response,body){
													if(!err){
														resolve(body.id);
													}
													else
													{
														reject(err);
													};
												});
											}));
										};

										//insert new products to cmg 
										Promise.all(insertRequests).then(function(response){
											
											if(typeof(response) == 'object')
											{	
												response = response.sort(function(a,b){return a-b});

												for(var i in insertRooms){
													insertRooms[i].push(response[i]);
												};
											}

											connection.query("INSERT INTO cmg_rate_plans_rooms(room_id, rate_plan_id, cmg_id) VALUES ?",[insertRooms],
											function(err,results){
												if(err){
													fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
													fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
													return res.sendStatus(500);
												}
												else
												{	
													
												}
											});

										});

										
										
									}



								// update insert or delete booking rule if exists
								if(ratePlanUpdate.hasOwnProperty('bookingRule'))
								{ 	

									if(ratePlanUpdate.bookingRule.instruction == "create")
									{
										bookingRule[0].unshift(ratePlanUpdate.id);

										connection.query("INSERT INTO cmg_booking_rules (rate_plan_id, type, days, date_from_one, date_to_one, date_from_two, date_to_two) VALUES ?",[bookingRule],
											function(err,results){
												if(err){
													fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
													fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
													return res.sendStatus(500);
												}
												else
												{
													res.sendStatus(200);
												}
											});
									}

									else if(ratePlanUpdate.bookingRule.instruction == "update")
									{	
										bookingRule[0].push(ratePlanUpdate.id);
										
										connection.query("UPDATE cmg_booking_rules SET type = ?, days = ?, date_from_one = ?, date_to_one = ?, date_from_two = ?, date_to_two = ? WHERE rate_plan_id = ?",
											[bookingRule[0][0], bookingRule[0][1], bookingRule[0][2], bookingRule[0][3], bookingRule[0][4], bookingRule[0][5], bookingRule[0][6]],
											function(err,response){
												if(err){
													fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
													fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
													return res.sendStatus(500);
												}
												else
												{
													res.sendStatus(200);
												}
											});
									}

									else if(ratePlanUpdate.bookingRule.instruction == "delete")
									{	

										connection.query("DELETE FROM cmg_booking_rules WHERE rate_plan_id = ?",ratePlanUpdate.id, function(err,response){
											if(err){
												fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
												fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
												return res.sendStatus(500);
											}
											else
											{
												res.sendStatus(200);
											}
										})
									}
								}
								else
								{
									//finally...
									res.sendStatus(200);
								}
								
							}
						});
					}
				});
			}
			else
			{
					fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
			};
		});

		

},

	deleteRatePlan: function(req,res){

		var ratePlanToDelete = req.body;
		
		var options = cmg.ratePlanDelete(ratePlanToDelete.cmgId);
   	
   		var productRequests = [];

		// firts fetch cmg_id of products reletated to rate plan and delete them before rate plan itself 
		connection.query("SELECT cmg_id FROM cmg_rate_plans_rooms WHERE rate_plan_id = ? ", ratePlanToDelete.id,function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
			else
			{	
				for(var i in results)
				{
					var options = cmg.deleteProduct(results[i].cmg_id);

					productRequests.push(new Promise(function(resolve,reject){

						request(options,function(err,response,body){
							
							if(!err && response.statusCode == 204){
								resolve(body);
							}
							else
							{	
								fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								reject(err);
							};
						});

					}));
				};

				 //delete all products from cmg related to rate plan
				Promise.all(productRequests).then(function(response){


				connection.query("DELETE FROM cmg_rate_plans WHERE id = ?",ratePlanToDelete.id,function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{
					connection.query("DELETE FROM cmg_rate_plans_rooms WHERE rate_plan_id = ?", ratePlanToDelete.id, function(err,results){
						if(err){
							fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
							fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
							return res.sendStatus(500);
						}
						else
						{	
							
							connection.query("DELETE FROM cmg_rates WHERE rate_plan = ? and hotel_id = ?", [ratePlanToDelete.name, ratePlanToDelete.hotel_id],
								function(err,results){
									if(err){
										fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
										fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
										return res.sendStatus(500);
									}
									else
									{
										connection.query("DELETE FROM cmg_booking_rules WHERE rate_plan_id = ?", ratePlanToDelete.id, function(err,results){
											if(err){
												fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/ratePlan \n',{'flag': 'a'});
												fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
												return res.sendStatus(500);
											}
											else
											{
												return res.sendStatus(200);
											}
										});
									};
								});	
						}

					});
				}
			});

		});

				// });

				
			};
		});

			

},



}
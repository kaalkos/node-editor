var fs = require('fs');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require('express');

var config = require('../config.json');
var cmg = require('./cmg.js');

var request = require('request');
var Promise = require('promise');

var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(typeof config.database.ssl != "undefined") {
	config.database.ssl.ca = fs.readFileSync(config.database.ssl.caPath);
	config.database.ssl.cert = fs.readFileSync(config.database.ssl.certPath);
	config.database.ssl.key = fs.readFileSync(config.database.ssl.keyPath);	
}

var connection = mysql.createConnection(config.database);

connection.connect(function(err){
	if(err){
		fs.writeFileSync('error.log',new Date() + ' database connection error \n',{'flag': 'a'});
		fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
		return console.log(err);
	}
});


module.exports = 
{	
	
	fetchRates: function(req,res){
		var ratesInfo = req.body;

		connection.query('SELECT * FROM cmg_rates WHERE hotel_id = ? AND rate_plan = ? AND room = ? ORDER BY date', [ratesInfo.hotel_id, ratesInfo.ratePlan, ratesInfo.room],
			function(err,results){
				
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /user/rates \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{	
					var rates = {};
					var days = [];

					for(var i in results)
					{
						var temp = results[i].date;
						rates[temp] = {};
						rates[temp].adultAdditional = results[i].adult_additional;
						rates[temp].adultMin = results[i].adult_minimum;
						rates[temp].adultStd = results[i].adult_standard;
						rates[temp].childAdditional = results[i].child_additional;
						rates[temp].closedToArrival = !!results[i].close_arrival;
						rates[temp].closedToDeparture = !!results[i].close_departure;
						rates[temp].dayOfWeek = results[i].day;
						rates[temp].lengthOfStay = results[i].min_stay + "-" + results[i].max_stay;
						rates[temp].price = results[i].room_price;
						rates[temp].stopSale = !!results[i].stop_sale;
						rates[temp].status = 'fetched';
						rates[temp].cmg_id = results[i].cmg_id;
						rates[temp].realDate = temp;

						days.push(temp);

					}

					res.send({rates: rates, days: days});
				}
			});
		
	},


	fetchRatePlans: function(req,res){
		var id = req.body.id;
		
		var ratePlanId = [];

		connection.query("SELECT * FROM cmg_rate_plans WHERE hotel_id = ? ", id, function(err,results){
			if(err){return console.log(err)}
			else
			{
				var ratePlan = results;
				
				var query = "SELECT * FROM cmg_rate_plans_rooms WHERE rate_plan_id = ? ";
				for(var i in ratePlan)
				{
					ratePlanId.push(ratePlan[i].id);
					if(i > 0)
					{
						query += "OR rate_plan_id = ? ";
					}
				}
				
				connection.query(query, ratePlanId ,function(err,roomResults){
					if(err){ return console.log(err)}
					else
					{	
						for(var i in ratePlan)
						{	
							
							ratePlan[i].rooms = [];
							for(var n in roomResults)
							{	

								if(ratePlan[i].id == roomResults[n].rate_plan_id)
								{
									ratePlan[i].rooms.push(roomResults[n]);
								}

							}
						}

						res.send(ratePlan);
					}
				});
			}
		});

},

	fetchSeasons: function(req,res){
		hotel_id = req.body.id;

		connection.query("SELECT * FROM cmg_seasons WHERE hotel_id = ?", hotel_id, function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /user/seasons \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
			else
			{
				res.send(results);
			}

		});

},
	
	fetchDeposits: function(req,res){
	
	connection.query('SELECT * FROM cmg_deposits WHERE hotel_id = ?', req.body.id,function(err,results){
		if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /user/deposits \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
		res.send(results);
	});
},

	fetchCancellations: function(req,res){
	
		connection.query('SELECT * FROM cmg_cancellations WHERE hotel_id = ?', req.body.id,function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /user/cancellations \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
			res.send(results);
		});
},

	sendSeason: function(req,res){
		var season = req.body;
		
		var seasonToSend = [];
		seasonToSend[0] = 
		[
			season.name,
			season.from,
			season.to,
			season.id
		];
		

		connection.query("INSERT INTO cmg_seasons(name, from_date, to_date, hotel_id) VALUES ?", [seasonToSend], 
			function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/seasons \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
			}
			else
			{
				res.sendStatus(200);
			}		
		});

},

	deleteSeason: function(req,res){
		var season = req.body;
		
		connection.query("DELETE FROM cmg_seasons WHERE hotel_id = ? and name = ? ", [season.id, season.name], function(err,results){
			if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseDelete/seasons \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
			else
			{	
				res.sendStatus(200);
			}
		});
},

	updateSeason: function(req,res){
		var season = req.body;
		
		connection.query("UPDATE cmg_seasons SET name = ?, from_date = ?, to_date = ? WHERE hotel_id = ? AND name = ? ",[season.name, season.from, season.to, season.id, season.oldName],
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/seasons \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{
					res.sendStatus(200);
				}
			});
},
	
	updateRatePlan: function(req,res){
		var ratePlanUpdate = req.body;

		var roomsId = [];
		var roomsIdFromDB = []
		

		for(var i in ratePlanUpdate.rooms)
		{
			roomsId.push(parseInt(ratePlanUpdate.rooms[i].id));
		}

		connection.query("UPDATE cmg_rate_plans SET name = ?, meal = ?, cancellation = ?, deposit = ?, pricing = ? WHERE id = ?",
			[ratePlanUpdate.name, ratePlanUpdate.meal, ratePlanUpdate.cancellation.id, ratePlanUpdate.deposit.id, ratePlanUpdate.per, ratePlanUpdate.id],
			function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{	
					connection.query("SELECT room_id FROM cmg_rate_plans_rooms WHERE rate_plan_id = ?", ratePlanUpdate.id, function(err,results){
						if(err){
								fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								return res.sendStatus(500);
							}
						else
						{
							for(var i in results)
							{
								roomsIdFromDB.push(results[i].room_id);
							}

							//compare arrays of rooms and send a query to update relations in database

								var diffOne = roomsId.filter(x => roomsIdFromDB.indexOf(x) < 0);
								var diffTwo = roomsIdFromDB.filter(x => roomsId.indexOf(x) < 0);
						
								var insertRooms = [];
								var deleteRooms = [];
								

								for(var i in diffOne)
								{	
									insertRooms[i] = [];
									insertRooms[i].push(diffOne[i]);
									insertRooms[i].push(ratePlanUpdate.id);
								}

								for(var i in diffTwo)
								{
									deleteRooms[i] = [];
									deleteRooms[i].push(diffTwo[i]);
									deleteRooms[i].push(ratePlanUpdate.id);
								}

								if(diffTwo.length > 0)
								{	
									connection.query("DELETE FROM cmg_rate_plans_rooms WHERE (room_id, rate_plan_id) IN (?)",[deleteRooms],
										function(err,results){
												if(err){
													fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
													fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
													return res.sendStatus(500);
												}
												else
												{	
													
												}
											});
								}

								if(insertRooms.length > 0)
								{	
									
									connection.query("INSERT INTO cmg_rate_plans_rooms(room_id, rate_plan_id) VALUES ?",[insertRooms],
										function(err,results){
											if(err){
												fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/ratePlan \n',{'flag': 'a'});
												fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
												return res.sendStatus(500);
											}
											else
											{	
												
											}
									});
								}

							//finally...
							res.sendStatus(200);
						}
					});
				}
			});

},

	sendRates: function(req,res){


		var data = req.body;
		
		var toCreate = [];

		var rateFunctions = [];
	
		for(var i in data.rates)
		{	
			var stay = data.rates[i].lengthOfStay.split('-');
			var temp = {};
			temp.minStay = stay[0];
			temp.maxStay = stay[1];

			toCreate[i] =
			[
			data.rates[i].realDate,
			data.rates[i].dayOfWeek,
			data.rates[i].price,
			data.rates[i].adultMin,
			data.rates[i].adultStd,
			data.rates[i].adultAdditional,
			data.rates[i].childAdditional,
			data.rates[i].closedToArrival,
			data.rates[i].closedToDeparture,
			data.rates[i].stopSale,
			temp.minStay,
			temp.maxStay,
			data.hotel,
			data.ratePlan,
			data.room.name

			];
		};

		for(var i in toCreate){
			var options = cmg.ratesCreate(toCreate[i][0],toCreate[i][2], data.room.cmg_id);
			rateFunctions.push(new Promise(function(resolve,reject){
				request(options,function(err,response,body){
					
					if(!err){
						
						var temp = body._links.self.href;
						temp = temp.substring(temp.lastIndexOf("/") + 1);
						
						resolve(temp);
					}
					else
					{
						reject(err);
					};
				});

			}));
		};

		Promise.all(rateFunctions)
		.then(function(response){

			if(typeof(response) == 'object'){
				response = response.sort(function(a,b){return a-b})
			};

			for(var i in toCreate){
				toCreate[i].push(response[i]);
			};

			var query = "INSERT INTO cmg_rates(date,day,room_price,adult_minimum,adult_standard,adult_additional,child_additional,close_arrival,close_departure,stop_sale,min_stay,max_stay,hotel_id, rate_plan, room, cmg_id) VALUES ?";

			connection.query(query,[toCreate],function(err,results){
				if(err){
					fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/rates \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					return res.sendStatus(500);
				}
				else
				{	
					res.sendStatus(200);
				}
			});

		});

		
},

	updateRates: function(req,res)
	{	
	
		var data = req.body;
		
		var toUpdate = [];

		var rateFunctions = [];

		for(var i in data.rates)
		{
			var stay = data.rates[i].lengthOfStay.split('-');
			var temp = {};
			temp.minStay = stay[0];
			temp.maxStay = stay[1];

			toUpdate[i] = 
			[
				data.rates[i].price,
				data.rates[i].adultMin,
				data.rates[i].adultStd,
				data.rates[i].adultAdditional,
				data.rates[i].childAdditional,
				data.rates[i].closedToArrival,
				data.rates[i].closedToDeparture,
				data.rates[i].stopSale,
				temp.minStay,
				temp.maxStay,
				data.hotel,
				data.ratePlan,
				data.room.name,
				data.rates[i].realDate
			];
		};

		for(var i in toUpdate){
			var options = cmg.ratesUpdate(toUpdate[i][13],toUpdate[i][0],data.room.cmg_id,data.rates[i].cmg_id);

			rateFunctions.push(new Promise(function(resolve,reject){

				request(options,function(err,response,body){
					
					if(!err && response.statusCode == 200){
						resolve(body);
					}
					else{
						reject(err);
					}
				});	

			}));
			
		};

		Promise.all(rateFunctions)
		.then(function(response){

			var query = "UPDATE cmg_rates  SET room_price = ?, adult_minimum = ?, adult_standard = ?, adult_additional = ?, child_additional = ?, close_arrival = ?, close_departure = ?, stop_sale = ?, min_stay = ?, max_stay = ? WHERE hotel_id = ? AND rate_plan = ? AND room = ? AND date = ?";

			toUpdate.forEach(singleArr => {
				connection.query(query,singleArr, function(err,results){
					if(err){
						fs.writeFileSync('error.log',new Date() + ' API error /databaseUpdate/rates \n',{'flag': 'a'});
						fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
						return res.sendStatus(500);
					}
					else
					{
						
					}
				});
			});

			res.sendStatus(200);

		});

		
	}
	

}
//
// some of the logic responsible for connection with cmg system is here
//
var bodyParser = require('body-parser');
var express = require('express');
var app = express();

var request = require('request');
var Promise = require('promise');

var fs = require('fs');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


// create code for cmg API 
function createCode(name)
{
	var temp = name.split(' ');
	var code = '';

	for(var i in temp)
	{	
		code += temp[i].charAt(0).toUpperCase();
	};
	return code;
};

// fetch current cmg token from external file 
var token = JSON.parse(fs.readFileSync('token.json','utf8'));


// check every 4 minutes if token is expired and then change it to new one
setInterval(function(){

	var currentTime = new Date().getTime();

	if(currentTime >= token.expired)
	{	
		var options = {
		headers:
		{
		 	'Content-Type': 'application/json'
		},
		body: 
		{
			'username': 'kostek',
			'password': 'laosfries'
		},
		uri: 'http://cm-gateway-staging.chicretreats.com/authentication',
		method: 'POST',
		json: true
		};

		// fetch a new token from cmg 
		request(options, function(err,response,body){
			
			if(!err && response.statusCode == 200){

				var tokenInfo = 
				{
					"token": body,
					"expired": currentTime + 849312000 
				};


				//save new token in a file
				fs.writeFileSync('token.json', JSON.stringify(tokenInfo),'utf8');


				// pass new token to global variable to make it avaible for all api-cmg functions 
				token = tokenInfo;
			}
			else
			{
				fs.writeFileSync('error.log',new Date() + ' CMG token fetching problem - http://cm-gateway-staging.chicretreats.com/authentication \n',{'flag': 'a'});
				fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
				return false;
			};
		});	


	}
	else
	{	
		return true;
	};

},240000);



module.exports = 
{	
	createAvailabilities: function(date,units,room)
	{
		var options = {
			url: "http://cm-gateway-staging.chicretreats.com/api/availabilities",
			method: "POST",
			headers:{
				'content-type': 'application/json',
		        'Authorization': 'Basic YXBpdXNlcjpNczRFM3dJMzBi'
			},
			body:{
				availableDate: date,
				units: units,
				open:  "true",
				room: "http://cm-gateway.chicretreats.com/api/rooms/" + room
			},
			json: true
		};


		return options 
	},
	
	fetchRooms: function(req,res)
	{	

		var uid = req.body.uid;

		var options = {
			headers: {
		     'Content-Type': 'text/plain',
		     'Authorization': 'Basic YXBpdXNlcjpNczRFM3dJMzBi'
		    },
		    uri: 'http://cm-gateway-staging.chicretreats.com/api/rooms/search/findByPropertyCode?propertyCode=' + uid,
		    method: 'GET'	
		};

		request(options, function(err,response,body){

				if(!err && response.statusCode == 200)
				{	
					var cmg = JSON.parse(body);
					
					res.send(cmg._embedded.rooms);
				}
				else
				{	
					fs.writeFileSync('error.log',new Date() + ' CMG connection error - /rooms/search/findByPropertyCode?propertyCode= \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					res.sendStatus(500);
				};
		});

	},

	fetchProperty: function(req,res)
	{	
		var uid = req.body.uid;
		
		var options = {
			headers: {
		      'Content-Type': 'text/plain',
		      'Authorization': 'Bearer ' + token.token
		    },
		    uri: 'http://cm-gateway-staging.chicretreats.com/api/properties/search/findByCode?code=' + uid,
		    method: 'GET'	
		};

		request(options, function(err,response,body){
				if (!err && response.statusCode == 200) {

				   var cmg = JSON.parse(body);
				 
				   res.send(String(cmg.id));
				 }
				 else
				 {
				 	fs.writeFileSync('error.log',new Date() + ' CMG connection error - /properties/search/findByCode?code= \n',{'flag': 'a'});
					fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
					res.sendStatus(500);
				 };
		});

		
	},

	ratePlanCreate: function(ratePlanName, uid)
	{	
	
		var options = {
			url: "http://cm-gateway-staging.chicretreats.com/api/ratePlans",
			method: "POST",
			headers:{
				'content-type': 'application/json',
		        'Authorization': 'Bearer ' + token.token
			},
			body:{
				name: ratePlanName,
				code: createCode(ratePlanName),
				property:  "//api/properties/" + uid
			},
			json: true
		};

		return options;
	},

	ratePlanUpdate: function(id, newName)
	{
		var options = {
			url: "http://cm-gateway-staging.chicretreats.com/api/ratePlans/" + id,
			method: "PUT",
			headers:{
				'content-type': 'application/json',
		        'Authorization': 'Bearer ' + token.token
			},
			body:{
				id: id,
				name: newName,
				code: createCode(newName)
			},
			json: true
		};

		return options;
	},

	ratePlanDelete: function(ratePlan)
	{	
		
			var options = {
				url: 'http://cm-gateway-staging.chicretreats.com/api/ratePlans/' + ratePlan,
				method: 'DELETE',
				headers:{
				'content-type': 'application/json',
			    'Authorization': 'Bearer ' + token.token
				}
			};

			return options;
	
	},

	ratesCreate: function(date,amount,product)
	{	

		var options = {
				url: "http://cm-gateway-staging.chicretreats.com/api/rates",
				method: "POST",
				headers:{
					'content-type': 'application/json',
			    	'Authorization': 'Bearer ' + token.token
				},
				body:{
					"avail_date": date,
					"base_amount_after_tax": amount,
					"product": "//api/products/" + product
				},
				json: true	
			};
			
			return options;
		
	},

	ratesUpdate: function(date,amount,product,id)
	{
		var options = {
				url: "http://cm-gateway-staging.chicretreats.com/api/rates/" + id,
				method: "PUT",
				headers:{
					'content-type': 'application/json',
			    	'Authorization': 'Bearer ' + token.token
				},
				body:{
					"avail_date": date,
					"base_amount_after_tax": amount,
					"product": "//api/products/" + product
				},
				json: true	
			};
			
			return options;
	},

	productCreate: function(property,ratePlan,room)
	{	
	
			var options = {
				url: "http://cm-gateway-staging.chicretreats.com/api/products",
				method: "POST",
				headers:{
					'content-type': 'application/json',
			        'Authorization': 'Bearer ' + token.token
				},
				body:{
					"property": '//api/properties/' + property,
					"ratePlan": '//api/ratePlans/' + ratePlan,
					"room": '//api/rooms/' + room
				},
				json: true
			};

			return options;
			
	
		
	},

	fetchProducts: function(id)
	{	
		

			var options = {
				url: 'http://cm-gateway-staging.chicretreats.com/api/ratePlans/' + id + '/products',
				method: "GET",
				headers:{
					'content-type': 'application/json',
			        'Authorization': 'Bearer ' + token.token
				}
			};
		
			return options;		
	},

	deleteProduct: function(id)
	{	
	
			var options = {
				url: 'http://cm-gateway-staging.chicretreats.com/api/products/' + id,
				method: "DELETE",
				headers:{
					'content-type': 'application/json',
			        'Authorization': 'Bearer ' + token.token
				}
			};
			
			return options; 
		
	}

};
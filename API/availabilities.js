var fs = require('fs');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var express = require('express');

var config = require('../config.json');
var cmg = require('./cmg.js');

var app = express();

var request = require('request');
var Promise = require('promise');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if(typeof config.database.ssl != "undefined") {
	config.database.ssl.ca = fs.readFileSync(config.database.ssl.caPath);
	config.database.ssl.cert = fs.readFileSync(config.database.ssl.certPath);
	config.database.ssl.key = fs.readFileSync(config.database.ssl.keyPath);	
}

var connection = mysql.createConnection(config.database);

connection.connect(function(err){
	if(err){
		fs.writeFileSync('error.log',new Date() + ' database connection error \n',{'flag': 'a'});
		fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
		return console.log(err);
	}
});

module.exports = {

	createAvailability: function(req,res){
						
						
		var data = req.body;
		var avlbToSend = [];
		var avlbToCreate = [];
		var avlbToUpdate = [];

		var avlbFunctions = [];
		var avlbDbFunctions = [];

		for(var i in data)
		{
			var temp = Object.keys(data[i])[0];

			for(var x in data[i][temp])
			{
				avlbToSend.push({
					date:  data[i][temp][x].date,
					room:  data[i][temp][x].uid,
					units: data[i][temp][x].availability.available
				});
			};
		};

		for(var i in avlbToSend)
		{
			avlbDbFunctions.push(new Promise(function(resolve,reject){

				// check in database if availability already exists and fetch its cmg id
				connection.query("SELECT cmg_id FROM cmg_availabilities WHERE date = ? AND uid = ?",[avlbToSend],
					function(err,results){
						if(err){
							fs.writeFileSync('error.log',new Date() + ' database error /databaseSend/availabilities \n',{'flag': 'a'});
							fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
							return res.sendStatus(500);
						}
						else
						{

						}
					});

			}));
		};

		for(var i in avlbToSend)
		{
			var options = cmg.createAvailabilities(avlbToSend[i].date,avlbToSend[i].units,avlbToSend[i].room);
			
			avlbFunctions.push(new Promise(function(resolve,reject){

				request(options,function(err,response,body){
					
					if(!err && response.statusCode == 201){

						// send avlb to local db in order to store it's cmg id if user will want to modify it
						var avlbToDb = [];
						avlbToDb[0] = [body.id,body.availableDate,body.units,body.open];
						
						connection.query("INSERT INTO cmg_availabilities(cmg_id,date_available,units,open) VALUES ?",[avlbToDb],
						function(err,results){
							if(err){
								fs.writeFileSync('error.log',new Date() + ' API error /databaseSend/availabilities \n',{'flag': 'a'});
								fs.writeFileSync('error.log', err + ' \n\n',{'flag': 'a'});
								return res.sendStatus(500);
							}
							else
							{
								resolve(body);
							}
						});
						
					}
					else
					{
						reject(err);
					}
				});

			}));
			
		};

		Promise.all(avlbFunctions)
		.then(function(response){
			
			res.sendStatus(200);
		})

	}

};